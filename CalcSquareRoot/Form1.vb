﻿'A program computes and displays the square root of a number:'

Imports System.Math
Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim num, sqr As Double
        num = InputBox("Enter Number: ")
        sqr = Sqrt(num)
        MsgBox("SQUARE ROOT = " & sqr)
    End Sub
End Class